module.exports = {
  Namespace: require('./namespace.model'),
  Room: require('./room.model'),
  User: require('./user.model'),
  Message: require('./message.model'),
};
