let namespaces;
let namespaceSockets = [];
let rooms = [];
let init = false;
let activeNsSocket;
let activeRoom;
let messages = [];

const ioClient = io({
  reconnection: false,
});

ioClient.on('connect', () => {
  console.log('Connexion ioClient OK');
});

ioClient.on('namespaces', (data) => {
  namespaces = data;
  for (let ns of namespaces) {
    const nsSocket = io(`/${ns._id}`);
    nsSocket.on('rooms', (data) => {
      rooms.push(...data);
      // Nous ne rentrerons dans ce bloc que pour le premier namespace
      // qui est connecté et a reçu l’événement rooms :
      if (!init) {
        // Nous passons init à true pour n’exécuter cela que pour le premier
        // namespace à être connecté :
        init = true;
        // Cela va permettre d’afficher les rooms du premier
        // namespace chargé qui sera sélectionné par défaut
        activateNamespace(nsSocket);
        // Nous pouvons afficher les namespaces ici car nous avons déjà
        // reçu tous les namespaces du serveur et que nous savons que nous
        //  avons reçu les rooms du namespace sélectionné par défaut
        displayNamespaces(namespaces, nsSocket.nsp);
      }
    });
    nsSocket.on('history', (data) => {
      messages = data;
      displayMessages(messages);
    });
    nsSocket.on('message', (data) => {
      messages.push(data);
      displayMessages(messages);
    });
    namespaceSockets.push(nsSocket);
  }
});

function activateRoom(room) {
  activeNsSocket.emit('joinRoom', room._id);
  activeRoom = room;
}

// Nous activons le premier namespace chargé, à savoir nous allons afficher
// les rooms de ce namespaces :
function activateNamespace(nsSocket) {
  // Nous conservons le namespace sélectionné dans une variable
  // cela sera utile pour la navigation entre les différents namespaces :
  activeNsSocket = nsSocket;
  // Nous récupérons la première room à afficher pour le namespace sélectionné :
  firstRoom = rooms.find((room) => `/${room.namespace}` === activeNsSocket.nsp && room.index === 0);
  // Nous allons mettre en place cette fonction dans la prochaine leçon, elle va
  // permettre l’affichage de toutes les rooms du namespace sélectionné :
  activateRoom(firstRoom);
  displayRooms(
    rooms.filter((room) => `/${room.namespace}` === activeNsSocket.nsp),
    firstRoom._id
  );
}

setTimeout(() => {
  console.log({
    namespaces,
    namespaceSockets,
    rooms,
    activeNsSocket,
    activateRoom,
    messages,
  });
}, 3000);
